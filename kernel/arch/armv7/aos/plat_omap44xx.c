/*
 * Reference implementation of AOS milestone 0, on the Pandaboard.
 */

/*
 * Copyright (c) 2009-2016 ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */

#include <kernel.h>

#include <assert.h>
#include <bitmacros.h>
#include <omap44xx_map.h>
#include <paging_kernel_arch.h>
#include <platform.h>
#include <serial.h>

#define MSG(format, ...) printk( LOG_NOTE, "OMAP44xx: "format, ## __VA_ARGS__ )
volatile int* uart3 = (int*) 0x48020000; //uart 3 start address
volatile char* uart3C = (char *) 0x48020000; //uart3 char
volatile int* uartLsr = (int*) 0x48020014; //uart line status register bit 5 needs to = 1
volatile int* ledptr = (int*) 0x4A310134;
volatile int* dataout = (int*) 0x4A31013C;
volatile int counter = 0;

void blink_leds(void);

/* RAM starts at 2G (2 ** 31) on the Pandaboard */
lpaddr_t phys_memory_start= GEN_ADDR(31);

/*** Serial port ***/

unsigned serial_console_port= 2;

errval_t serial_init(unsigned port, bool initialize_hw) 
{
    /* XXX - You'll need to implement this, but it's safe to ignore the
     * parameters. */
    return SYS_ERR_OK;
};

void serial_putchar(unsigned port, char c) 
{
    int mask = (1 << 5);    //equal to x * 2^(5) = 32
    int bit = (*uartLsr & mask) >> 5; //look for the status register bit
    while(bit != 1) //if bit is nt available
    {
        bit = (*uartLsr & mask) >> 5; //look at bit again
    }
    *uart3 = c; //set it to character value
}

char serial_getchar(unsigned port) 
{
    while(!(*uartLsr & 1));
    return *uart3C;
}

/*** LED flashing ***/

void blink_leds(void) 
{
    int mask = 1 << 8;
    *ledptr &= ~(mask);
    for(int i = 0; i < 46000000; i++)
    {
        ++counter;
    }
    *dataout = *dataout ^ mask;
}
